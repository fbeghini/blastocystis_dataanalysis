for img in `ls *svg`; do inkscape --verb=FitCanvasToDrawing --verb=FileSave --verb=FileQuit $img; done

for file in `ls *svg`; do
	inkscape $file --export-png=png/$file.png -b white
done
