library(reshape2)
library(ggplot2)

lefse_in <- read.table("lefse_results/all_lefse.tsv", row.names = 1)
archaea <- data.frame(t(lefse_in[c(1,2),]),stringsAsFactors=F)
archaea <- rbind(archaea,c("Medium","0"))
archaea$name <- rownames(archaea)
archaea <- data.frame(archaea[order(archaea[,1]),])
archaea$k__Archaea<-as.double(archaea$k__Archaea)
colnames(archaea) <- c("status","RelativeAbundance","name")

archaea$name <- factor(archaea$name, as.character(archaea$name))
xcoord1 = round((match("Present", archaea$status) - match("Absent", archaea$status))/2)
xcoord2 = match("Medium", archaea$status) + round((length(archaea$status) - match("Present", archaea$status)) / 2)
svg("lefse_results/lefse_archaea.svg", width=20)
plot(
     ggplot(archaea, aes(x=name, y=RelativeAbundance)) + 
	     geom_bar(stat="identity", width=5) +
	     geom_vline(xintercept = match("Medium", archaea$status), colour="red", size=0.7) +
	     labs(title = "Archaea") + 
	     xlab("Samples") +
	     annotate("label", x = xcoord1, y = max(archaea$RelativeAbundance), label = "Absent") + 
	     annotate("label", x = xcoord2, y = max(archaea$RelativeAbundance), label = "Present") +
	     theme(axis.ticks = element_blank(), 
		   axis.text.x = element_blank() )
    )
dev.off()
