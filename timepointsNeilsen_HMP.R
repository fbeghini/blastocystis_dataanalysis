library(ggplot2)
library(reshape2)
library(grid)
library(gridExtra)

blasto_abundance <- aMaxBlasto
colnames(blasto_abundance) <- gsub("_mapped.bed", "", colnames(blasto_abundance))
oldn <- c("blastoRACandela.", "blastoRAchat.", "blastoRAhmp\\.", "blastoRAhmpii.", "blastoRAloman.", "blastoRAmetahit.", "blastoRAneilsen.","blastoRAquin.", "blastoRAt2dm.", "blastoRAtito.", "blastoRAwt2d.", "blastoRAzeller.")
newn <- c("Candela_Africa:", "Chatelier_gut_obesity:", "hmp:", "hmpii:", "Loman2013_EcoliOutbreak_DNA_HiSeq:", "metahit:", "Neilsen_genome_assembly:", "Quin_gut_liver_cirrhosis:", "t2dmeta:", "Tito_subsistence_gut:", "WT2D:", "Zeller_fecal_colorectal_cancer:")

for(i in c(1:length(oldn))){
	colnames(blasto_abundance) <- gsub(oldn[i], newn[i], colnames(blasto_abundance))
}

rm(oldn)
rm(newn)
colnames(blasto_abundance) <- gsub("\\.", "-", colnames(blasto_abundance))
blasto_abundance <- blasto_abundance[, sort(colnames(blasto_abundance))]
blasto_abundance["Blastocystis sp.", ] <- colSums(blasto_abundance)

hmptimepoints<-read.csv(file = pipe("cut -f1,2,3 /home//francesco.beghini/hmptimepoints"), sep = "\t", col.names = c("sample","subject", "timepoint"), header = F)

hmp_present <- paste("hmp",hmptimepoints$sample, sep=":")

#filter only present
hmptpa <- blasto_presence[,hmp_present]
hmptpa[hmptpa<threshold] <- NA
hmptpa <- hmptpa[-8,!apply(is.na(hmptpa),2,all)]
hmptpa <- data.frame(t(blasto_abundance[-8,colnames(hmptpa)]))
hmptpa[, "sample"] <- gsub("hmp:","",rownames(hmptpa))
hmptpa <- merge(hmptpa, hmptimepoints)
hmptpa <- melt(hmptpa, id.vars=c("subject","timepoint", "sample"))
hmptpa$timepoint <- as.character(hmptpa$timepoint)


for(s in split(hmptpa,f=hmptpa$subject)){
	svg(paste("hmp_timepoints/", unique(s$subject),".svg", sep=""))
	print(ggplot(s) +
	aes(x=variable, y=value, group=timepoint, fill=timepoint) + 
	geom_bar( position="dodge", stat='identity') + 
	xlab("Subtype") + 
	ylab("Relative abundance") +
	ggtitle(unique(s$subject)))
	dev.off()
}



neilsentimepoints<-read.csv("/home//francesco.beghini/neilsentimepoints", sep = "\t", col.names = c("sample","subject"), header = F)
neilsentimepoints[,"timepoint"]<-unlist(lapply(as.character(neilsentimepoints$sample), function(x){strsplit(x,"_")[1][[1]][3]}))


neilsen_present <- paste("Neilsen_genome_assembly", neilsentimepoints$sample, sep=":")

neilsentpa <- data.frame(t(blasto_abundance[-8,neilsen_present]))
neilsentpa[, "sample"] <- gsub("Neilsen_genome_assembly:","",rownames(neilsentpa))
neilsentpa <- merge(neilsentpa, neilsentimepoints)

neilsentpa <- melt(neilsentpa, id.vars=c("subject","timepoint", "sample"))
neilsentpa$timepoint <- as.character(neilsentpa$timepoint)
neilsentpa$subject <- factor(neilsentpa$subject)

for(s in split(neilsentpa,f=neilsentpa$subject)){
	svg(paste("neilsen_timepoints/",unique(s$subject),".svg", sep=""))
	print(ggplot(s) +
	aes(x=variable, y=value, group=timepoint, fill=timepoint) + 
	geom_bar( position="dodge", stat='identity') + 
	xlab("Subtype") + 
	ylab("Relative abundance") +
	ggtitle(unique(s$subject)))
	dev.off()
}

load("timepoints")
hmp_abundance <- data.frame()
hmp_abundance["ST4_WR1", "hmp:SRS013476"]  <- blasto_abundance["ST4_WR1", "hmp:SRS013476"] 
hmp_abundance["ST4_WR1", "hmp:SRS024132"] <- blasto_abundance["ST4_WR1", "hmp:SRS024132"]
hmp_abundance["ST2","hmp:SRS013687"] <- blasto_abundance["ST2","hmp:SRS013687"]
hmp_abundance["ST2","hmp:SRS023914"] <- blasto_abundance["ST2","hmp:SRS023914"]
hmp_abundance[c("ST3","ST4_WR1","ST6") ,"hmp:SRS014235"]  <- blasto_abundance[c("ST3","ST4_WR1","ST6") ,"hmp:SRS014235"] 
hmp_abundance[c("ST3","ST4_WR1","ST6") ,"hmp:SRS019685"]  <- blasto_abundance[c("ST3","ST4_WR1","ST6") ,"hmp:SRS019685"] 
hmp_abundance[c("ST3","ST6"),"hmp:SRS015190"]  <- blasto_abundance[c("ST3","ST6"),"hmp:SRS015190"] 
hmp_abundance[c("ST3","ST6"),"hmp:SRS043411"]  <- blasto_abundance[c("ST3","ST6"),"hmp:SRS043411"] 
hmp_abundance["ST2","hmp:SRS051031"]  <- blasto_abundance["ST2","hmp:SRS051031"] 
hmp_abundance["ST2","hmp:SRS015663"]  <- blasto_abundance["ST2","hmp:SRS015663"] 

neilsen_abundance <- data.frame()
neilsen_abundance["ST2","Neilsen_genome_assembly:O2_UC12_0"] <- blasto_abundance["ST2","Neilsen_genome_assembly:O2_UC12_0"]
neilsen_abundance["ST2","Neilsen_genome_assembly:O2_UC12_2"] <- blasto_abundance["ST2","Neilsen_genome_assembly:O2_UC12_2"]
neilsen_abundance[c("ST4_WR1","ST7"),"Neilsen_genome_assembly:O2_UC13_0"] <- blasto_abundance[c("ST4_WR1","ST7"),"Neilsen_genome_assembly:O2_UC13_0"]
neilsen_abundance[c("ST4_WR1","ST7"),"Neilsen_genome_assembly:O2_UC13_2"] <- blasto_abundance[c("ST4_WR1","ST7"),"Neilsen_genome_assembly:O2_UC13_2"]
neilsen_abundance["ST9","Neilsen_genome_assembly:O2_UC18_0"] <- blasto_abundance["ST9","Neilsen_genome_assembly:O2_UC18_0"]
neilsen_abundance["ST9","Neilsen_genome_assembly:O2_UC18_2"] <- blasto_abundance["ST9","Neilsen_genome_assembly:O2_UC18_2"]
neilsen_abundance["ST3","Neilsen_genome_assembly:O2_UC24_0"] <- blasto_abundance["ST3","Neilsen_genome_assembly:O2_UC24_0"]
neilsen_abundance["ST3","Neilsen_genome_assembly:O2_UC24_2"] <- blasto_abundance["ST3","Neilsen_genome_assembly:O2_UC24_2"]
neilsen_abundance["ST8","Neilsen_genome_assembly:O2_UC29_0"] <- blasto_abundance["ST8","Neilsen_genome_assembly:O2_UC29_0"]
neilsen_abundance["ST8","Neilsen_genome_assembly:O2_UC29_2"] <- blasto_abundance["ST8","Neilsen_genome_assembly:O2_UC29_2"]
neilsen_abundance["ST3","Neilsen_genome_assembly:O2_UC35_0"] <- blasto_abundance["ST3","Neilsen_genome_assembly:O2_UC35_0"]
neilsen_abundance["ST3","Neilsen_genome_assembly:O2_UC35_2"] <- blasto_abundance["ST3","Neilsen_genome_assembly:O2_UC35_2"]
neilsen_abundance["ST3","Neilsen_genome_assembly:O2_UC40_0"] <- blasto_abundance["ST3","Neilsen_genome_assembly:O2_UC40_0"]
neilsen_abundance["ST3","Neilsen_genome_assembly:O2_UC40_2"] <- blasto_abundance["ST3","Neilsen_genome_assembly:O2_UC40_2"]
neilsen_abundance["ST4_WR1","Neilsen_genome_assembly:O2_UC50_0"] <- blasto_abundance["ST4_WR1","Neilsen_genome_assembly:O2_UC50_0"]
neilsen_abundance["ST4_WR1","Neilsen_genome_assembly:O2_UC50_2"] <- blasto_abundance["ST4_WR1","Neilsen_genome_assembly:O2_UC50_2"]
neilsen_abundance[c("ST3","ST4_WR1"), "Neilsen_genome_assembly:O2_UC51_0"] <- blasto_abundance[c("ST3","ST4_WR1"), "Neilsen_genome_assembly:O2_UC51_0"]
neilsen_abundance[c("ST3","ST4_WR1"), "Neilsen_genome_assembly:O2_UC51_2"] <- blasto_abundance[c("ST3","ST4_WR1"), "Neilsen_genome_assembly:O2_UC51_2"]
neilsen_abundance[c("ST3","ST4_WR1"), "Neilsen_genome_assembly:O2_UC52_0"] <- blasto_abundance[c("ST3","ST4_WR1"), "Neilsen_genome_assembly:O2_UC52_0"]
neilsen_abundance[c("ST3","ST4_WR1"), "Neilsen_genome_assembly:O2_UC52_2"] <- blasto_abundance[c("ST3","ST4_WR1"), "Neilsen_genome_assembly:O2_UC52_2"]
neilsen_abundance[c("ST4_WR1","ST7"), "Neilsen_genome_assembly:O2_UC55_0"] <- blasto_abundance[c("ST4_WR1","ST7"), "Neilsen_genome_assembly:O2_UC55_0"]
neilsen_abundance[c("ST4_WR1","ST7"), "Neilsen_genome_assembly:O2_UC55_2"] <- blasto_abundance[c("ST4_WR1","ST7"), "Neilsen_genome_assembly:O2_UC55_2"]
neilsen_abundance["ST2", "Neilsen_genome_assembly:O2_UC57_0"] <- blasto_abundance["ST2", "Neilsen_genome_assembly:O2_UC57_0"]
neilsen_abundance["ST2", "Neilsen_genome_assembly:O2_UC57_2"] <- blasto_abundance["ST2", "Neilsen_genome_assembly:O2_UC57_2"]
neilsen_abundance["ST3", "Neilsen_genome_assembly:O2_UC58_0"] <- blasto_abundance["ST3", "Neilsen_genome_assembly:O2_UC58_0"]
neilsen_abundance["ST3", "Neilsen_genome_assembly:O2_UC58_2"] <- blasto_abundance["ST3", "Neilsen_genome_assembly:O2_UC58_2"]
neilsen_abundance["ST2","Neilsen_genome_assembly:V1_UC14_0"] <- blasto_abundance["ST2","Neilsen_genome_assembly:V1_UC14_0"]
neilsen_abundance["ST2","Neilsen_genome_assembly:V1_UC14_1"] <- blasto_abundance["ST2","Neilsen_genome_assembly:V1_UC14_1"]
neilsen_abundance["ST3","Neilsen_genome_assembly:V1_UC17_0"] <- blasto_abundance["ST3","Neilsen_genome_assembly:V1_UC17_0"]
neilsen_abundance["ST3","Neilsen_genome_assembly:V1_UC17_2"] <- blasto_abundance["ST3","Neilsen_genome_assembly:V1_UC17_2"]
neilsen_abundance["ST3","Neilsen_genome_assembly:V1_UC21_0"] <- blasto_abundance["ST3","Neilsen_genome_assembly:V1_UC21_0"]
neilsen_abundance["ST3","Neilsen_genome_assembly:V1_UC21_4"] <- blasto_abundance["ST3","Neilsen_genome_assembly:V1_UC21_4"]
neilsen_abundance["ST4_WR1", "Neilsen_genome_assembly:V1_UC49_0"] <- blasto_abundance["ST4_WR1", "Neilsen_genome_assembly:V1_UC49_0"]
neilsen_abundance["ST4_WR1", "Neilsen_genome_assembly:V1_UC49_1"] <- blasto_abundance["ST4_WR1", "Neilsen_genome_assembly:V1_UC49_1"]

neilsen_subjects <-unique(as.character(neilsentimepoints[which(neilsentimepoints$sample %in% sapply(strsplit(colnames(neilsen_abundance),":"),"[",2)),"subject"]))
hmp_subjects<- unique(as.character(hmptimepoints[which(hmptimepoints$sample %in% sapply(strsplit(colnames(hmp_abundance),":"),"[",2)),"subject"]))
hmp_abundance[,"subtype"] <- rownames(hmp_abundance)
neilsen_abundance[,"subtype"] <- rownames(neilsen_abundance)
hmpmelt <- melt(hmp_abundance, na.rm=T)
neilsenmelt <- melt(neilsen_abundance, na.rm=T)

for(subject in hmp_subjects)
{
	ds <- melt(hmp_abundance[,c(paste("hmp", hmptimepoints[which(hmptimepoints$subject==subject),"sample"], sep=":"),"subtype")], na.rm=T)
	x <- hmptimepoints[which(hmptimepoints$subject==subject),c("sample","timepoint")]
	x$sample <- paste("hmp",x$sample,sep=":")
	ds <- merge(x,ds, by.x="sample", by.y="variable")
	svg(paste("hmp_timepoints/",subject,".svg", sep=""))
	print(
		ggplot(ds) +
		aes(x=as.character(timepoint), y=value, group=subtype, fill=subtype) + 
		geom_bar( position="dodge", stat='identity') + 
		xlab("Timepoint") + 
		ylab("Relative abundance") +
		ggtitle(subject)
	)
	dev.off()
}

for(subject in neilsen_subjects)
{
	ds <- melt(neilsen_abundance[,c(paste("Neilsen_genome_assembly", neilsentimepoints[which(neilsentimepoints$subject==subject),"sample"], sep=":"),"subtype")], na.rm=T)
	x <- neilsentimepoints[which(neilsentimepoints$subject==subject),c("sample","timepoint")]
	x$sample <- paste("Neilsen_genome_assembly",x$sample,sep=":")
	ds <- merge(x,ds, by.x="sample", by.y="variable")
	svg(paste("neilsen_timepoints/",subject,".svg", sep=""))
	print(
		ggplot(ds) +
		aes(x=timepoint, y=value, group=subtype, fill=subtype) + 
		geom_bar( position="dodge", stat='identity') + 
		xlab("Timepoint") + 
		ylab("Relative abundance") +
		ggtitle(subject)
	)
	dev.off()

}