dsPDoyle <- read.csv(file = "results_parasites/Doyle2015_infants_DNA/perc_merged.txt", header = TRUE, sep = '\t', row.names =1 )
dsPT2D <- read.csv(file = "results_parasites/t2dmeta/perc_merged.txt", header = TRUE, sep = '\t', row.names =1)
dsPhmp <- read.csv(file = "results_parasites/hmp/perc_merged.txt", header = TRUE, sep = '\t', row.names =1)
dsPhmpii <- read.csv(file = "results_parasites/hmpii/perc_merged.txt", header = TRUE, sep = '\t', row.names =1)
dsPQuin <- read.csv(file = "results_parasites/Quin_gut_liver_cirrhosis/perc_merged.txt", header = TRUE, sep = '\t', row.names =1)
dsPMetahit <- read.csv(file="results_parasites/metahit/perc_merged.txt", header = T, sep = '\t', row.names =1)
dsPNeilsen <- read.csv(file="results_parasites/Neilsen_genome_assembly/perc_merged.txt", header = T, sep = '\t', row.names =1)

dsPDoyle <- subset(dsPDoyle, grepl("refg_",rownames(dsPDoyle)))
dsPT2D <- subset(dsPT2D, grepl("refg_",rownames(dsPT2D)))
dsPhmp <- subset(dsPhmp, grepl("refg_",rownames(dsPhmp)))
dsPhmpii <- subset(dsPhmpii, grepl("refg_",rownames(dsPhmpii)))
dsPQuin <- subset(dsPQuin, grepl("refg_",rownames(dsPQuin)))
dsPMetahit <- subset(dsPMetahit, grepl("refg_",rownames(dsPMetahit)))
dsPNeilsen <- subset(dsPNeilsen, grepl("refg_",rownames(dsPNeilsen)))

dsFDoyle <- read.csv(file = "results_parasites/Doyle2015_infants_DNA/fold_merged.txt", header = TRUE, sep = '\t', row.names =1 )
dsFT2D <- read.csv(file = "results_parasites/t2dmeta/fold_merged.txt", header = TRUE, sep = '\t', row.names =1)
dsFhmp <- read.csv(file = "results_parasites/hmp/fold_merged.txt", header = TRUE, sep = '\t', row.names =1)
dsFhmpii <- read.csv(file = "results_parasites/hmpii/fold_merged.txt", header = TRUE, sep = '\t', row.names =1)
dsFQuin <- read.csv(file = "results_parasites/Quin_gut_liver_cirrhosis/fold_merged.txt", header = TRUE, sep = '\t', row.names =1)
dsFMetahit <- read.csv(file="results_parasites/metahit/fold_merged.txt", header = T, sep = '\t', row.names =1)
dsFNeilsen <- read.csv(file="results_parasites/Neilsen_genome_assembly/fold_merged.txt", header = T, sep = '\t', row.names =1)

dsFDoyle <- subset(dsFDoyle, grepl("refg_",rownames(dsFDoyle)))
dsFT2D <- subset(dsFT2D, grepl("refg_",rownames(dsFT2D)))
dsFhmp <- subset(dsFhmp, grepl("refg_",rownames(dsFhmp)))
dsFhmpii <- subset(dsFhmpii, grepl("refg_",rownames(dsFhmpii)))
dsFQuin <- subset(dsFQuin, grepl("refg_",rownames(dsFQuin)))
dsFMetahit <- subset(dsFMetahit, grepl("refg_",rownames(dsFMetahit)))
dsFNeilsen <- subset(dsFNeilsen, grepl("refg_",rownames(dsFNeilsen)))

blastoPdoyle <- read.csv("results_blasto/Doyle2015_infants_DNA/perc_merged.txt", header = T, sep = '\t', row.names =1)
blastoFdoyle <- read.csv("results_blasto/Doyle2015_infants_DNA/fold_merged.txt", header = T, sep = '\t', row.names =1)
blastoPt2dm <- read.csv("results_blasto/t2dmeta//perc_merged.txt", header = T, sep = '\t', row.names =1)
blastoFt2dm <- read.csv("results_blasto/t2dmeta//fold_merged.txt", header = T, sep = '\t', row.names =1)
blastoPhmp <- read.csv("results_blasto/hmp//perc_merged.txt", header = T, sep = '\t', row.names =1)
blastoFhmp <- read.csv("results_blasto/hmp//fold_merged.txt", header = T, sep = '\t', row.names =1)
blastoPhmpii <- read.csv("results_blasto/hmpii//perc_merged.txt", header = T, sep = '\t', row.names =1)
blastoFhmpii <- read.csv("results_blasto/hmpii//fold_merged.txt", header = T, sep = '\t', row.names =1)
blastoPquin <- read.csv("results_blasto/Quin_gut_liver_cirrhosis/perc_merged.txt", header = T, sep = '\t', row.names =1)
blastoFquin <- read.csv("results_blasto/Quin_gut_liver_cirrhosis/fold_merged.txt", header = T, sep = '\t', row.names =1)
blastoPmetahit <- read.csv("results_blasto/metahit/perc_merged.txt", header = T, sep = '\t', row.names =1)
blastoFmetahit <- read.csv("results_blasto/metahit/fold_merged.txt", header = T, sep = '\t', row.names =1)
blastoPneilsen <- read.csv("results_blasto/Neilsen_genome_assembly/perc_merged.txt", header = T, sep = '\t', row.names =1)
blastoFneilsen <- read.csv("results_blasto/Neilsen_genome_assembly/fold_merged.txt", header = T, sep = '\t', row.names =1)
blastoPzeller <- read.csv("results_blasto/Zeller_fecal_colorectal_cancer/perc_merged.txt", header = T, sep = '\t', row.names =1)
blastoFzeller <- read.csv("results_blasto/Zeller_fecal_colorectal_cancer/fold_merged.txt", header = T, sep = '\t', row.names =1)
blastoPchat <- read.csv("results_blasto/Chatelier_gut_obesity/perc_merged.txt", header = T, sep = '\t', row.names =1)
blastoFchat <- read.csv("results_blasto/Chatelier_gut_obesity/fold_merged.txt", header = T, sep = '\t', row.names =1)
blastoPwt2d <- read.csv("results_blasto/WT2D/perc_merged.txt", header = T, sep = '\t', row.names =1)
blastoFwt2d <- read.csv("results_blasto/WT2D/fold_merged.txt", header = T, sep = '\t', row.names =1)
blastoPtito <- read.csv("results_blasto/Tito_subsistence_gut/perc_merged.txt", header = T, sep = '\t', row.names =1)
blastoFtito <- read.csv("results_blasto/Tito_subsistence_gut/fold_merged.txt", header = T, sep = '\t', row.names =1)
blastoPloman <- read.csv("results_blasto/Loman2013_EcoliOutbreak_DNA_HiSeq/perc_merged.txt", header = T, sep = '\t', row.names =1)
blastoFloman <- read.csv("results_blasto/Loman2013_EcoliOutbreak_DNA_HiSeq/fold_merged.txt", header = T, sep = '\t', row.names =1)
blastoPCandela <- read.csv("results_blasto/Candela_Africa/perc_merged.txt", header = T, sep = '\t', row.names =1)
blastoFCandela <- read.csv("results_blasto/Candela_Africa/fold_merged.txt", header = T, sep = '\t', row.names =1)

blastoRAt2dm <- read.csv("results_blasto/t2dmeta//abundances_merged.txt", header = T, sep = '\t', row.names =1)
blastoRAhmp <- read.csv("results_blasto/hmp//abundances_merged.txt", header = T, sep = '\t', row.names =1)
blastoRAhmpii <- read.csv("results_blasto/hmpii//abundances_merged.txt", header = T, sep = '\t', row.names =1)
blastoRAquin <- read.csv("results_blasto/Quin_gut_liver_cirrhosis/abundances_merged.txt", header = T, sep = '\t', row.names =1)
blastoRAmetahit <- read.csv("results_blasto/metahit/abundances_merged.txt", header = T, sep = '\t', row.names =1)
blastoRAneilsen <- read.csv("results_blasto/Neilsen_genome_assembly/abundances_merged.txt", header = T, sep = '\t', row.names =1)
blastoRAzeller <- read.csv("results_blasto/Zeller_fecal_colorectal_cancer/abundances_merged.txt", header = T, sep = '\t', row.names =1)
blastoRAchat <- read.csv("results_blasto/Chatelier_gut_obesity/abundances_merged.txt", header = T, sep = '\t', row.names =1)
blastoRAwt2d <- read.csv("results_blasto/WT2D/abundances_merged.txt", header = T, sep = '\t', row.names =1)
blastoRAtito <- read.csv("results_blasto/Tito_subsistence_gut/abundances_merged.txt", header = T, sep = '\t', row.names =1)
blastoRAloman <- read.csv("results_blasto/Loman2013_EcoliOutbreak_DNA_HiSeq/abundances_merged.txt", header = T, sep = '\t', row.names =1)
blastoRACandela <- read.csv("results_blasto/Candela_Africa/abundances_merged.txt", header = T, sep = '\t', row.names =1)

blastoPdoyle <- blastoPdoyle[grepl("^ST.$|ST4_WR1", rownames(blastoPdoyle)),]
blastoFdoyle <- blastoFdoyle[grepl("^ST.$|ST4_WR1", rownames(blastoFdoyle)),]
blastoPt2dm <- blastoPt2dm[grepl("^ST.$|ST4_WR1", rownames(blastoPt2dm)),]
blastoFt2dm <- blastoFt2dm[grepl("^ST.$|ST4_WR1", rownames(blastoFt2dm)),]
blastoPhmp <- blastoPhmp[grepl("^ST.$|ST4_WR1", rownames(blastoPhmp)),]
blastoFhmp <- blastoFhmp[grepl("^ST.$|ST4_WR1", rownames(blastoFhmp)),]
blastoPhmpii <- blastoPhmpii[grepl("^ST.$|ST4_WR1", rownames(blastoPhmpii)),]
blastoFhmpii <- blastoFhmpii[grepl("^ST.$|ST4_WR1", rownames(blastoFhmpii)),]
blastoPquin <- blastoPquin[grepl("^ST.$|ST4_WR1", rownames(blastoPquin)),]
blastoFquin <- blastoFquin[grepl("^ST.$|ST4_WR1", rownames(blastoFquin)),]
blastoPmetahit <- blastoPmetahit[grepl("^ST.$|ST4_WR1", rownames(blastoPmetahit)),]
blastoFmetahit <- blastoFmetahit[grepl("^ST.$|ST4_WR1", rownames(blastoFmetahit)),]
blastoPneilsen <- blastoPneilsen[grepl("^ST.$|ST4_WR1", rownames(blastoPneilsen)),]
blastoFneilsen <- blastoFneilsen[grepl("^ST.$|ST4_WR1", rownames(blastoFneilsen)),]
blastoPzeller <- blastoPzeller[grepl("^ST.$|ST4_WR1", rownames(blastoPzeller)),]
blastoFzeller <- blastoFzeller[grepl("^ST.$|ST4_WR1", rownames(blastoFzeller)),]
blastoPchat <- blastoPchat[grepl("^ST.$|ST4_WR1", rownames(blastoPchat)),]
blastoFchat <- blastoFchat[grepl("^ST.$|ST4_WR1", rownames(blastoFchat)),]
blastoPwt2d <- blastoPwt2d[grepl("^ST.$|ST4_WR1", rownames(blastoPwt2d)),]
blastoFwt2d <- blastoFwt2d[grepl("^ST.$|ST4_WR1", rownames(blastoFwt2d)),]
blastoPtito <- blastoPtito[grepl("^ST.$|ST4_WR1", rownames(blastoPtito)),]
blastoFtito <- blastoFtito[grepl("^ST.$|ST4_WR1", rownames(blastoFtito)),]
blastoPloman <- blastoPloman[grepl("^ST.$|ST4_WR1", rownames(blastoPloman)),]
blastoFloman <- blastoFloman[grepl("^ST.$|ST4_WR1", rownames(blastoFloman)),]
blastoPCandela <- blastoPCandela[grep("^ST.$|ST4_WR1",rownames(blastoPCandela)),]
blastoFCandela <- blastoFCandela[grep("^ST.$|ST4_WR1",rownames(blastoFCandela)),]

maxDoyle<-getTopResults(list(dsPDoyle,dsFDoyle))
maxT2D<-getTopResults(list(dsPT2D,dsFT2D))
maxHMP<-getTopResults(list(dsPhmp,dsFhmp))
maxHMPII<-getTopResults(list(dsPhmpii,dsFhmpii))
maxQuin<-getTopResults(list(dsPQuin,dsFQuin))
maxMetahit<-getTopResults(list(dsPMetahit,dsFMetahit))
maxNeilsen<-getTopResults(list(dsPNeilsen,dsFNeilsen))

write.csv(maxDoyle,paste(deparse(substitute(maxDoyle)),".csv", sep = ""))
write.csv(maxT2D,paste(deparse(substitute(maxT2D)),".csv", sep = ""))
write.csv(maxHMP,paste(deparse(substitute(maxHMP)),".csv", sep = ""))
write.csv(maxHMPII,paste(deparse(substitute(maxHMPII)),".csv", sep = ""))
write.csv(maxQuin,paste(deparse(substitute(maxQuin)),".csv", sep = ""))
write.csv(maxMetahit,paste(deparse(substitute(maxMetahit)),".csv", sep = ""))
write.csv(maxNeilsen,paste(deparse(substitute(maxNeilsen)),".csv", sep = ""))

maxBlastoDoyle<-getTopResults(list(blastoPdoyle, blastoFdoyle))
maxBlastoT2D<-getTopResults(list(blastoPt2dm,blastoFt2dm))
maxBlastoHmp<-getTopResults(list(blastoPhmp,blastoFhmp))
maxBlastoHmpii<-getTopResults(list(blastoPhmpii,blastoFhmpii))
maxBlastoQuin<-getTopResults(list(blastoPquin,blastoFquin))
maxBlastoMetahit<-getTopResults(list(blastoPmetahit,blastoFmetahit))
maxBlastoNeilsen<-getTopResults(list(blastoPneilsen,blastoFneilsen))
maxBlastoZeller<-getTopResults(list(blastoPzeller,blastoFzeller))
maxBlastoChat<-getTopResults(list(blastoPchat,blastoFchat))
maxBlastoWT2D<-getTopResults(list(blastoPwt2d,blastoFwt2d))


write.table(format(t(maxBlasto), scientific = F), file = "blasto_presence.csv", sep = ";", dec = ",", qmethod = "double")