#!/usr/bin/enc Rscript
library(ggplot2)
library(reshape2)
args <- commandArgs(TRUE)

if (length(args)==0) {
    stop("Insert input file name")
} else {
    infile <- args[1]  
}
if (length(args[2])==0) {
    limit <-  NULL
} else {
    limit <- as.numeric(strsplit(args[2],"-")[[1]])
}
if (length(args[3])==0) {
    outname <- paste(stringi::stri_rand_strings(n=1, length = 8),"svg", sep = '.')
} else {
    outname <- paste(args[3],"svg", sep = '.')
}
print(args[3])
# infile <- "ST2_dist,ST3_dist,ST4_dist"
filein  <- strsplit(infile, ',')[[1]]
x <- lapply(filein, read.delim, skip=1, stringsAsFactors=F)
x <- lapply(x, function(x) x[,-ncol(x)])
vals <- lapply(x, function(x) {
    lapply(x[upper.tri(x)], function(cell) {
        eval(parse(text = cell))
        })
    })
ds <- data.frame()
for(i in 1:length(vals)) {
    vals.l <- unlist(vals[[i]])
    ds <- rbind(ds, data.frame(value = vals.l, input = rep(filein[i], length(vals.l))))
}
print("Outfile: ")
print(outname)
svg(filename = outname)
print( 
	ggplot(ds, aes(value, fill = input, colour = input)) + 
	geom_density(alpha = 0.1) +
	labs(x = "SNP rate %", y="Density") +  
	scale_x_continuous(limits = c(0,max(ds$value)+0.05)) + 
	theme_classic() + 
	labs(title=infile)
)
dev.off()
